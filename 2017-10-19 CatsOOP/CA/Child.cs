﻿using System;

namespace CA
{
    public class Child : Man
    {
        public Child(string name) : base(name)
        {
            Console.WriteLine("Ребенок родился");
        }
    }
}