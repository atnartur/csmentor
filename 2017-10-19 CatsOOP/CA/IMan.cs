﻿namespace CA
{
    public interface IMan
    {
        void IncreasePlesure(); // увеличить удовольствие
        void FeedTheCat(ICat cat); // накормить кота
    }
}