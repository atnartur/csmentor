﻿namespace CA
{
    public class Man : IMan
    {
        public int Pleasure { get; private set; }
        public string Name { get; private set; }

        public Man(string name)
        {
            Name = name;
        }

        public void IncreasePlesure()
        {
            Pleasure++;
        }

        public void FeedTheCat(ICat cat)
        {
            cat.Eat();
        }
    }
}