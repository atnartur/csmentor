﻿using System;
using System.Security.Policy;

namespace CA
{
    public class Cat : ICat, IHealtheble
    {
        public enum Color { Black, White, Yellow }

        public Cat(Color hairColor, Color eyesColor, int height, int weight, int fullness, string name)
        {
            HairColor = hairColor;
            EyesColor = eyesColor;
            Height = height;
            Weight = weight;
            Fullness = fullness;
            Name = name;
        }

        public Color HairColor { get; private set; } // цвет шерсти
        public Color EyesColor { get; private set; } // цвет глаз
        public int Height { get; private set; } // рост
        public int Weight { get; private set; } // вес
        public int Fullness { get; private set; } // сытость
        public string Name { get; private set; } // имя

        public void SayMeow()
        {
            Console.WriteLine("Мяу!");
        }

        public void MakeMur(IMan man = null)
        {
            if (man != null)
            {
                man.IncreasePlesure();
            }
        }
        
        public void MakeMur(Child child = null)
        {
            if (child != null)
            {
                child.IncreasePlesure();
            }
        }

//        void MakeMur(Man man = null); // мурлыкнуть
        public void Eat()
        {
            Fullness++;
            Console.WriteLine("Я сыт на " + Fullness + " из 10");
        }

        public bool Heal()
        {
            Console.WriteLine("Кот вылечен");
            return true;
        }
    }
}