﻿using System;

namespace CA
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // ООП
            Cat myCat = new Cat(Cat.Color.Black, Cat.Color.Yellow, 10, 5, 5, "Шока");
            Console.WriteLine(myCat.Name);
            myCat.SayMeow();
            
            IHealtheble catForVeterinar = new Cat(Cat.Color.Black, Cat.Color.Yellow, 10, 5, 5, "Шока");
            catForVeterinar.Heal();
            
            ICat myCat2 = new Cat(Cat.Color.Black, Cat.Color.Yellow, 10, 5, 5, "Шока");
            myCat2.Eat();
            
            Man man = new Man("Хозяин");
            man.FeedTheCat(myCat2);
            
            myCat2.MakeMur(man);
            Console.WriteLine(man.Pleasure);
            Console.WriteLine(man.Name);
           
            Child child = new Child("Ребенок");
            myCat2.MakeMur(child);
            Console.WriteLine(child.Pleasure);
            
            // текст. Посчитать колво слов, которые начинаются 
            // с большой буквы
            string text =
                "Возможность использования объектов в обобщенном виде без учета деталей каждого из них. Коту неважно, для кого он будет мурлыкать. И ребенок, и взрослый могут получать удовольствие от этого Берем фотоаппараты, смартфоны и камерафоны и используем их как фотоаппараты, потому что они все имеют хоть какую-нибудь камеру";

            string[] words = text.Split(' ');

            foreach (var word in words)
            {
                var firstLetter = word[0];
                bool check = 
                    firstLetter >= 'A' && firstLetter <= 'Z' || 
                    firstLetter >= 'А' && firstLetter <= 'Я';
                Console.WriteLine(check + " - " + word);
            }

            // разворот всего массива
            for (int i = 0; i < words.Length; i++)
            {
                var tmp = words[i];
                words[i] = words[words.Length - i - 1];
                words[words.Length - i - 1] = tmp;
            }
            
            foreach (var word in words)
            {
                Console.Write(word + " ");
            }
            
            
            // разворот промежутка
            for (int i = 4; i < 4 + 4; i++)
            {
                var tmp = words[i];
                words[i] = words[words.Length - i - 1];
                words[words.Length - i - 1] = tmp;
            }
            
            foreach (var word in words)
            {
                Console.Write(word + " ");
            }
        }
    }
}