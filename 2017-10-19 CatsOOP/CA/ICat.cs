﻿namespace CA
{
    public interface ICat
    {
        void SayMeow();
        void MakeMur(IMan man = null); // мурлыкнуть
        void Eat();
    }
}