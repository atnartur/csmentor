﻿using System;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void NegativeSign()
        {
            var bigint = new BigInt.BigInt(-5);
            Assert.AreEqual(BigInt.BigInt.SignType.Negative, bigint.Sign, "неправильный знак");
            Assert.AreEqual("-5", bigint.ToString());
        }
        
        [Test]
        public void PositiveSign()
        {
            var bigint = new BigInt.BigInt(5);
            Assert.AreEqual(BigInt.BigInt.SignType.Positive, bigint.Sign);
            Assert.AreEqual("5", bigint.ToString());
        }

        [Test]
        public void Sum()
        {
            var a = new BigInt.BigInt(5);
            var b = new BigInt.BigInt(6);
            var res = a + b;
            Console.WriteLine(res);
            Assert.AreEqual(res.ToString(), "11");       
        }
    }
}