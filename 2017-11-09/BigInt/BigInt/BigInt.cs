﻿using System.Text;

namespace BigInt
{
    /// <summary>
    /// Реализация BigInt
    /// </summary>
    public class BigInt
    {
        /// <summary>
        /// Типы знаков числа
        /// </summary>
        public enum SignType { Positive, Negative }
        
        /// <summary>
        /// Массив цифр
        /// </summary>
        private byte[] _digits;
        
        /// <summary>
        /// Знак числа
        /// </summary>
        public SignType Sign { get; private set; }

        /// <summary>
        /// Длина числа
        /// </summary>
        public int Length { get; private set; }
        
        /// <summary>
        /// Строковый конструктор
        /// </summary>
        public BigInt(string str)
        {
            int add = 0; // счетчик по символам числа
            
            // Если строчка начинается с - => отрицательное число
            if (str[0] == '-')
            {
                Sign = SignType.Negative;
                add++;
            }
            else
                Sign = SignType.Positive;
            
            Length = str.Length - add; // устанавливаем длину числа
            _digits = new byte[Length]; // создаем массив для цифр
            
            // запсиываем числа в обратном порядке. Таким образом удобнее считать столбиком
            for (int i = 0; i < Length; i++)
                _digits[_digits.Length - i - 1] = (byte)(str[i + add] - '0');
        }

        /// <summary>
        /// Числовой конструктор
        /// </summary>
        /// <param name="num"></param>
        public BigInt(int num)
        {
            Sign = num < 0 ? SignType.Negative : SignType.Positive;
            if (num < 0)
                num *= -1;
            Length = num.ToString().Length;
            _digits = new byte[Length];

            for (int i = 0; i < Length; i++)
            {
                _digits[i] = (byte) (num % 10);
                num /= 10;
            }
        }
        
        /// <summary>
        /// Возвращает строковое представление числа
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            if (Sign == SignType.Negative)
                str.Append('-');
            for (int j = Length - 1; j >= 0; j--)
                str.Append(_digits[j]);
            return str.ToString();
        }
        
        /// <summary>
        /// Подсчет количества цифр без лидирующих нулей
        /// </summary>
        private int GetLength()
        {
            int length = _digits.Length - 1;
            while (length > 0 && _digits[length] == 0)
                length--;
            return length + 1;
        }
        
        /// <summary>
        /// Сравнение по модулю
        /// </summary>
        private int AbsCompare(BigInt other)
        {
            if (Length != other.Length)
                return Length - other.Length;
            for (int i = Length - 1; i >= 0; i--)
                if (_digits[i] != other._digits[i])
                    return _digits[i] - other._digits[i];
            return 0;
        }
        
        /// <summary>
        /// Изменение размера массива чисел
        /// </summary>
        /// <param name="newLength"></param>
        private void Resize(int newLength)
        {
            Length = newLength;
            byte[] oldDigits = _digits;
            _digits = new byte[Length];
            oldDigits.CopyTo(_digits, 0);
        }
        
        /// <summary>
        /// Складывание чисел
        /// </summary>
        private static BigInt SumImpl(BigInt a, BigInt b)
        {
            // перекладываем в a наибольшее число
            if (a.AbsCompare(b) < 0)
            {
                BigInt tmp = a;
                a = b;
                b = tmp;
            }
            
            BigInt result = new BigInt(a._digits.Length + 1);
            a._digits.CopyTo(result._digits, 0);

            int p = 0; // добавок
            // цикл по числам наименьшего числа
            for (int i = 0; i < b.Length; i++)
            {
                result._digits[i] = (byte)(a._digits[i] + b._digits[i] + p); // складываем 2 числа и добавок
                p = result._digits[i] / 10; // все, что больше 10 - в добавок (целочисленное деленеи)
                result._digits[i] %= 10; // все, что меньше 10 - в результат (отстаток от целочисленного деления)
            }
            
            // оставшиеся числа просто спускаем в результат
            for (int j = b.Length; p > 0; j++)
            {
                if(j > result._digits.Length - 1) // если вышли за границы массива - увеличиваем размер
                    result.Resize(result._digits.Length + 1);

                result._digits[j] = (byte)(result._digits[j] + p); // добавок мог остаться с предыдщего шага, поэтому
                p = result._digits[j] / 10; // не забываем про него
                result._digits[j] %= 10;
            }
            result.Length = result.GetLength();
            return result;
        }

        /// <summary>
        /// Addition of two BigInt numbers.
        /// </summary>
        public static BigInt Sum(BigInt a, BigInt b)
        {
            // @TODO: реализовать работу со знаками
            return SumImpl(a, b);
        }
        
        private static BigInt Subtract(BigInt n1, BigInt n2)
        {
            throw new System.NotImplementedException();
        }
        private static BigInt Subtract(BigInt n1, int n2)
        {
            throw new System.NotImplementedException();
        }
        
        private static BigInt Mult(BigInt n1, BigInt n2)
        {
            throw new System.NotImplementedException();
        }
        private static BigInt Mult(BigInt n1, int n2)
        {
            throw new System.NotImplementedException();
        }
        
        private static BigInt Div(BigInt n1, int n2)
        {
            throw new System.NotImplementedException();
        }
        private static BigInt Div(BigInt n1, BigInt n2)
        {
            throw new System.NotImplementedException();
        }
        
        private static BigInt Mod(BigInt n1, BigInt n2)
        {
            throw new System.NotImplementedException();
        }
        
        public static BigInt operator +(BigInt n1, BigInt n2) => Sum(n1, n2);
        public static BigInt operator ++(BigInt n1) => Sum(n1, new BigInt(1));
        public static BigInt operator -(BigInt n1, BigInt n2) => Subtract(n1, n2);
        public static BigInt operator -(BigInt n1, int n2)    => Subtract(n1, n2);
        public static BigInt operator *(BigInt n1, BigInt n2) => Mult(n1, n2);
        public static BigInt operator *(BigInt n1, int n2)    => Mult(n1, n2);
        public static BigInt operator /(BigInt n1, int n2)    => Div(n1, n2);
        public static BigInt operator /(BigInt n1, BigInt n2) => Div(n1, n2);
        public static BigInt operator %(BigInt n1, BigInt n2) => Mod(n1, n2);        
    }
}