﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var a = "0.0001".Replace(".", ",");
            var b = "0,0001";
            double a1;
            double b1;

            double.TryParse(a, out a1);
            double.TryParse(b, out b1);
            Console.WriteLine(a1);
            Console.WriteLine(b1);
        }
    }
}