﻿using System;

namespace ConsoleApplication1
{
    internal class Program
    {
        enum Chairs { Old, Office, Taburet} 
        
        public static int secondNumber = 6;

        void a(Chairs b)
        {
            Console.WriteLine(b == Chairs.Office);
            Console.WriteLine(b == Chairs.Taburet);
            Console.WriteLine(b == Chairs.Old);
        }
        
        public static void Main(string[] args)
        {
            string str = "      Введение в информатику. Лекция 19    ";
            
            // Посчитать количество слов
            
            string[] arrayOfWords = str.Trim().Split(' ');
            Console.WriteLine("Количество слов: " + arrayOfWords.Length);
            
            // четность количества слов в введенном тексте. 
            // Вывести результаты на экран
//            if (check)
//            {
//                Console.WriteLine("В тексте четное количество слов");
//            }
//            else
//            {
//                Console.WriteLine("В тексте нечетное количество слов");
//            }

//            bool check = arrayOfWords.Length % 2 == 0;
//            Console.WriteLine("В тексте " + check  + "четное количество слов");
            Console.WriteLine("В тексте " + (arrayOfWords.Length % 2 == 0 ? "" : "не")  + "четное количество слов");

            

            Console.WriteLine(str.IndexOf("информатику"));

            arrayOfWords = str.Split(' ');
            foreach (var word in arrayOfWords)
            {
                Console.WriteLine(word);   
            }
            Console.WriteLine(String.Join(" ", arrayOfWords));
            Console.WriteLine(str.ToLower());
            Console.WriteLine(str.ToUpper());
            Console.WriteLine(str.Trim(new []{ '9', ' ' }));
        }

        public static int Sum(int a, int b)
        {
            SayThatOperationIsRunning();
            return a + b;
        }

        private static void SayThatOperationIsRunning()
        {
            Console.WriteLine("идет операция сложения");
        }
    }
}