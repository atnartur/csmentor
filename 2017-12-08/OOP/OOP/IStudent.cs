﻿namespace OOP
{
    public interface IStudent
    {
        void LiveWithoutSleep();
        void Study(string subject);
        void Eat();
        void ToBeLate();
        void PassExamine(string subject);
    }
}