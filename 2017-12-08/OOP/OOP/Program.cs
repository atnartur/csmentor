﻿namespace OOP
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Man man = new Man();
            man.Eat();
            man.PassExamine("дискретка");
            
            IStudent student = new Man();
            student.Eat();
            student.Study("алгем");
            student.LiveWithoutSleep();
            
            IMan man2 = new Man();
            man2.Eat();
            man2.Work();
            
            Child child = new Child();
            child.ComeInTime();
            child.Eat();
        }
    }
}