﻿using System;

namespace OOP
{
    public class Man : IStudent, IMan
    {
        public void LiveWithoutSleep()
        {
            Console.WriteLine("Я не спал!");
        }

        public virtual void Eat()
        {
            Console.WriteLine("Я ем!");
        }
        
        void IMan.Eat()
        {
            Console.WriteLine("Я кушаю!");
        }

        void IStudent.Eat()
        {
            Console.WriteLine("Я кушаю дошик!");
        }

        public void Study(string subject)
        {
            Console.WriteLine("Я учу " + subject);
        }

        public void ToBeLate()
        {
            Console.WriteLine("Я опоздал!!!");
        }

        public void Relax()
        {
            Console.WriteLine("Балдеж");
        }

        public void ComeInTime()
        {
            Console.WriteLine("Я успел!");
        }

        public void Love()
        {
            Console.WriteLine("Я влюблен!");
        }

        public void Work()
        {
            Console.WriteLine("Keep calm and work hard");
        }

        public void PassExamine(string subject)
        {
            Console.WriteLine("Я сдал " + subject + "!");
        }
    }
}