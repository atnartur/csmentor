﻿using System;

namespace OOP
{
    public class Child : Man
    {
        public void Play()
        {
            Console.WriteLine("Я играю!");
        }
        public override void Eat()
        {
            Console.WriteLine("Я кушаю конфетку!");
        }
    }
}